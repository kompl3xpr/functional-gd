# Functional.gd

**Functional.gd** is a GDScript library that simplifies working with sequences and functional programming concepts while employing the powerful technique of lazy evaluation. It offers a variety of functions and classes for transforming and manipulating sequences, enabling you to write more expressive and efficient code.

## Installation

To use the **Functional.gd** library in your Godot project, follow these steps:

1. Download the `Functional.gd` file from this repository.
2. Place the `Functional.gd` file in your Godot project's script directory.
3. You can now use the `Functional` class in your GDScript files.


## Examples

```gdscript
func _ready():
	var sub1 = fn.compose(fn.curry, fn.flip).call(fn.sub).call(1)
	
	var n = fn.irange(0, 99999)\
		.filter(func(x): return x % 2 == 0)\
		.take(10)\
		.map(sub1)\
		.enumerate()\
		.map(fn.id_with(func(x): print(x)))\
		.unzip().snd\
		.reduce(fn.add)
	assert(n == 80)
	
	var flag = [false]
	var m = fn.irange(0, 10)\
		.map(fn.always(1))\
		.map(func(x): return fn.ArrayIter.new([x]))\
		.flatten()\
		.intersperse(func():
			flag[0] = !flag[0]
			return 3 if flag[0] else 5)\
		.map(fn.id_with(func(x): print(x)))\
		.chain(fn.ArrayIter.new([2]))\
		.any(func(x): return x == 2)
	assert(m)
```

## Usage

The Functional library leverages lazy evaluation, a technique where operations on sequences are computed only when necessary. This approach optimizes memory usage and improves performance, making it a powerful tool for working with sequences. Here's a comprehensive list of available functions and classes that harness the power of lazy evaluation:

| Function/Variable          | Description                                      |
| -------------------------- | ------------------------------------------------ |
| `map(f: Callable) -> Map`   | Transform elements in a sequence.               |
| `filter(p: Callable) -> Filter` | Filter elements based on a condition.          |
| `enumerate() -> Enumerate` | Enumerate elements with their index.           |
| `take(n: int) -> Take`     | Take a specific number of elements.            |
| `take_while(p: Callable) -> TakeWhile` | Take elements while a condition is met. |
| `peekable() -> Peekable`   | Create a peekable version of the sequence.      |
| `chain(other: LazyIter) -> Chain` | Chain two sequences together.               |
| `zip(other: LazyIter) -> Zip` | Combine two sequences element-wise.          |
| `flatten() -> Flatten`     | Flatten a sequence of sequences.               |
| `intersperse(factory: Callable) -> Intersperse` | Intersperse elements with a factory. |
| `unzip() -> Pair`          | Unzip a sequence of pairs into two sequences.  |
| `skip(n: int) -> LazyIter`  | Skip the first `n` elements in a sequence.     |
| `skip_while(pred: Callable) -> LazyIter` | Skip elements while a condition is met. |
| `reduce(f: Callable)`     | Reduce the sequence to a single value.         |
| `fold(init, f: Callable)` | Fold the sequence with an initial value.       |
| `to_arr() -> Array`       | Convert the sequence to an array.              |
| `to_dict() -> Dictionary` | Convert the sequence to a dictionary.          |
| `for_each(f: Callable)`   | Apply a function to each element in the sequence. |
| `group_by(f: Callable) -> Dictionary` | Group elements by a key function.       |
| `group_by_fst() -> Dictionary` | Group pairs by their first element.        |
| `any(pred: Callable) -> bool` | Check if any element satisfies a condition. |
| `all(pred: Callable) -> bool` | Check if all elements satisfy a condition. |

| Utility Function/Variable  | Description                                      |
| -------------------------- | ------------------------------------------------ |
| `neg` | Function to negate a number. |
| `fnot` | Function to negate a boolean value. |
| `add` | Function to add two numbers. |
| `sub` | Function to subtract one number from another. |
| `mul` | Function to multiply two numbers. |
| `div` | Function to divide one number by another. |
| `mod` | Function to calculate the remainder. |
| `fst` | Function to extract the first element of a pair. |
| `snd` | Function to extract the second element of a pair. |
| `id` | Identity function (returns its input). |
| `id_with(f: Callable)` | Identity function with a side effect. |
| `always(x: Any) -> Callable` | Returns a function that always returns `x`. |
| `flip(f: Callable) -> Callable` | Returns a function that flips the arguments of `f`. |
| `compose(f: Callable, g: Callable) -> Callable` | Returns a function that composes two functions, `f` and `g`. |
| `curry(f: Callable, arity: int = 2) -> Callable` | Curries a function `f` with a specific arity. |
| `id_with(f: Callable)` | Identity function with a side effect. |
| `irange(begin: int, end: int, step: int = 1) -> IRange` | Creates a range of integers from `begin` to `end` with an optional step. |

| Class               | Description                                          |
| ------------------- | ---------------------------------------------------- |
| `LazyIter`          | Base class for working with lazy sequences.          |
| `DictIter`          | Subclass for working with dictionaries.             |
| `ArrayIter`         | Subclass for working with arrays.                   |
| `Map`               | Class for mapping elements in a sequence.           |
| `Filter`            | Class for filtering elements based on a condition.  |
| `Enumerate`         | Class for enumerating elements with their index.    |
| `Take`              | Class for taking a specific number of elements.     |
| `TakeWhile`         | Class for taking elements while a condition is met. |
| `Peekable`          | Class for creating a peekable version of the sequence. |
| `Chain`             | Class for chaining two sequences together.          |
| `Zip`               | Class for combining two sequences element-wise.     |
| `Flatten`           | Class for flattening a sequence of sequences.       |
| `Intersperse`       | Class for interspersing elements with a factory.    |
| `IRange`            | Class for creating a range of integer values.      |
| `Pair`              | Simple class for representing pairs of elements.    |

These functions and classes enable you to perform various operations on sequences, making your code more expressive and functional.


## License

This library is released under the MIT license. See the [LICENSE](LICENSE) file for more details.

## Contributing

We welcome contributions! If you'd like to contribute to this project or report issues, please see the [CONTRIBUTING](CONTRIBUTING.md) file for guidelines.

## Author

This library was developed by Kompl3xpr.