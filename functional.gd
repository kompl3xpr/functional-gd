extends Object
class_name Functional


class LazyIter:
	func next():
		push_error("call abstract method `next()`")

	func has_next() -> bool:
		push_error("call abstract method `has_next()`")
		return false

	func map(f: Callable) -> Map:
		return Map.new(self, f)

	func filter(p: Callable) -> Filter:
		return Filter.new(self, p)

	func enumerate() -> Enumerate:
		return Enumerate.new(self)

	func take(n: int) -> Take:
		return Take.new(self, n)

	func take_while(p: Callable) -> TakeWhile:
		return TakeWhile.new(self, p)

	func peekable() -> Peekable:
		return Peekable.new(self)

	func chain(other: LazyIter) -> Chain:
		return Chain.new(self, other)

	func zip(other: LazyIter) -> Zip:
		return Zip.new(self, other)

	func flatten() -> Flatten:
		return Flatten.new(self)

	func intersperse(factory: Callable) -> Intersperse:
		return Intersperse.new(self, factory)

	func unzip() -> Pair:
		var a: Array = self.to_arr()
		var b = a.duplicate()
		return Pair.new(
			a.map(func(x): return x.fst),
			b.map(func(x): return x.snd))

	func skip(n: int) -> LazyIter:
		for i in range(n):
			self.next()
		return self

	func skip_while(pred: Callable) -> LazyIter:
		var p = Peekable.new(self)
		while p.has_next():
			if pred.call(p.peek()):
				p.next()
		return p

	func reduce(f: Callable):
		if not self.has_next():
			return null
		return self.fold(self.next(), f)

	func fold(init, f: Callable):
		var result = init
		while self.has_next():
			result = f.call(result, self.next())
		return result

	func to_arr() -> Array:
		return self.fold([], func(arr: Array, item):
			arr.append(item)
			return arr)

	func to_dict() -> Dictionary:
		return self.fold({}, func(dict: Dictionary, item: Pair):
			dict[item.fst] = item.snd
			return dict)

	func for_each(f: Callable):
		while self.has_next():
			f.call(self.next())

	func group_by(f: Callable) -> Dictionary:
		var result = {}
		self.for_each(func(v):
			var k = f.call(v)
			if result.has(k):
				result[k].append(v)
			else:
				result[k] = [v])
		return result
		
	func group_by_fst() -> Dictionary:
		var result = {}
		self.for_each(func(x: Pair):
			if result.has(x.fst):
				result[x.fst].append(x.snd)
			else:
				result[x.fst] = [x.snd])
		return result

	func any(pred: Callable) -> bool:
		while self.has_next():
			if pred.call(self.next()):
				return true
		return false

	func all(pred: Callable) -> bool:
		while self.has_next():
			if not pred.call(self.next()):
				return false
		return true

class DictIter extends LazyIter:
	var __keys: Array
	var __inner: Dictionary
	var __idx: int

	func _init(dict: Dictionary):
		__keys = dict.keys()
		__inner = dict
		__idx = 0

	func has_next() -> bool:
		return __idx < __inner.size()

	func next():
		if self.has_next():
			return null
		var k = __keys[__idx]
		__idx += 1
		return Pair.new(k, __inner[k])


class ArrayIter extends LazyIter:
	var __inner: Array
	var __idx: int

	func _init(arr: Array):
		__inner = arr
		__idx = 0

	func has_next() -> bool:
		return __idx < __inner.size()

	func next():
		var res = __inner[__idx] if self.has_next() else null
		__idx += 1
		return res


class Map extends LazyIter:
	var __it: LazyIter
	var __f: Callable

	func _init(iter: LazyIter, fn: Callable):
		__it = iter
		__f = fn

	func has_next() -> bool:
		return __it.has_next()

	func next():
		if __it.has_next():
			return __f.call(__it.next())
		else:
			return null


class Filter extends LazyIter:
	var __it: Peekable
	var __p: Callable

	func _init(iter: LazyIter, pred: Callable):
		__it = Peekable.new(iter)
		__p = pred

	func has_next() -> bool:
		while __it.has_next():
			if __p.call(__it.peek()):
				return true
			else:
				__it.next()
		return false

	func next():
		if self.has_next():
			return __it.next()
		else:
			return null


class Enumerate extends LazyIter:
	var __it: LazyIter
	var __index: int

	func _init(iter: LazyIter):
		__it = iter
		__index = 0

	func has_next() -> bool:
		return __it.has_next()

	func next():
		if not self.has_next():
			return null
		
		var item = __it.next()
		var result = Pair.new(__index, item)
		__index += 1
		return result


class Take extends LazyIter:
	var __it: LazyIter
	var __index: int
	var __size: int

	func _init(iter: LazyIter, n: int):
		__it = iter
		__index = 0
		__size = n

	func has_next() -> bool:
		return __index < __size and __it.has_next()

	func next():
		if not self.has_next():
			return null
		__index += 1
		return __it.next()


class TakeWhile extends LazyIter:
	var __it: Peekable
	var __end: bool
	var __p: Callable

	func _init(iter: LazyIter, pred: Callable):
		__it = Peekable.new(iter)
		__end = false
		__p = pred

	func has_next() -> bool:
		if not __it.has_next():
			return false
		return __p.call(__it.peek())

	func next():
		if self.has_next():
			return __it.next()
		else:
			return null


class Chain extends LazyIter:
	var __it1: LazyIter
	var __it2: LazyIter

	func _init(iter1: LazyIter, iter2: LazyIter):
		__it1 = iter1
		__it2 = iter2

	func has_next() -> bool:
		return __it1.has_next() or __it2.has_next()

	func next():
		if __it1.has_next():
			return __it1.next()
		elif __it2.has_next():
			return __it2.next()
		else:
			return null


class Zip extends LazyIter:
	var __it1: LazyIter
	var __it2: LazyIter

	func _init(iter1: LazyIter, iter2: LazyIter):
		__it1 = iter1
		__it2 = iter2

	func has_next() -> bool:
		return __it1.has_next() and __it2.has_next()

	func next():
		if self.has_next():
			return Pair.new(__it1.next(), __it2.next())
		else:
			return null

class Peekable extends LazyIter:
	var __last
	var __last_used: bool
	var __it: LazyIter

	func _init(iter: LazyIter):
		__it = iter
		__last_used = true

	func has_next() -> bool:
		if not __last_used:
			return true
		if not __it.has_next():
			return false
		__last = __it.next()
		__last_used = false
		return true

	func peek():
		if self.has_next():
			return __last
		else:
			return null

	func next():
		if self.has_next():
			__last_used = true
			return __last
		else:
			return null


class Flatten extends LazyIter:
	var __it = Peekable

	func _init(iter: LazyIter):
		__it = Peekable.new(iter)

	func has_next() -> bool:
		while __it.has_next():
			if not __it.peek().has_next():
				__it.next()
			else:
				return true
		return false

	func next():
		if self.has_next():
			return __it.peek().next()
		else:
			return null


class Intersperse extends LazyIter:
	var __it: LazyIter
	var __factory: Callable
	var __flag: bool

	func _init(iter: LazyIter, factory: Callable):
		__it = iter
		__factory = factory
		__flag = false

	func has_next() -> bool:
		return __it.has_next()

	func next():
		if not self.has_next():
			return null
		__flag = !__flag
		if __flag:
			return __it.next()
		else:
			return __factory.call()


static func irange(begin: int, end: int, step: int = 1) -> IRange:
	return IRange.new(begin, end, step)


class IRange extends LazyIter:
	var __current
	var __end
	var __step

	func _init(begin: int, end: int, step: int = 1):
		__current = begin
		__end = end
		__step = step

	func has_next() -> bool:
		if __step > 0:
			if __current >= __end:
				return false
		elif __current <= __end:
			return false
		return true

	func next():
		if not self.has_next():
			return null
			
		var result = __current
		__current += __step
		return result


class Pair:
	var fst
	var snd

	func _init(first, second):
		fst = first
		snd = second

	func _to_string():
		return "({0}, {1})".format([fst, snd])


static var id = func(x): return x

static func always(x):
	return func(_y): return x

static func flip(f: Callable):
	return func(x, y): return f.call(y, x)

static func compose(f: Callable, g: Callable):
	return func(x): return f.call(g.call(x))

static func curry(f: Callable):
	return func(x): return func(y): return f.call(x, y)

static func id_with(f: Callable):
	return func(x):
		f.call(x)
		return x

static var neg = func(x): return -x
static var fnot = func(x): return not x
static var add = func(x, y): return x + y
static var sub = func(x, y): return x - y
static var mul = func(x, y): return x * y
static var div = func(x, y): return x / y
static var mod = func(x, y): return x % y
static var fst = func(x: Pair): return x.fst
static var snd = func(x: Pair): return x.snd
